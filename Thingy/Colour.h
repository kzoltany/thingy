//
//  Colour.h
//  Thingy
//
//  Created by Charles Benedict on 27/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Colour : NSObject

@property (strong, nonatomic) NSArray *colours;

- (UIColor *)randomColor;

@end
