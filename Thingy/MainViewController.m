//
//  ViewController.m
//  Thingy
//
//  Created by Charles Benedict on 27/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

#import "MainViewController.h"
#import "Facts.h"
#import "Colour.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void) viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
  self.firstTime = false;
  self.factBook = [[Facts alloc] init];
  self.colourBook = [[Colour alloc] init];
  
  self.textLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 85, 250, 280)];
  self.textLabel.text = [self.factBook randomFact];
  self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
  self.textLabel.numberOfLines = 255;
  self.textLabel.textColor = [UIColor whiteColor];
  [self.view addSubview:self.textLabel];
  
  self.button = [[UIButton alloc] initWithFrame:CGRectMake(45, 437, 280, 80)];
  [self.button setTitleColor:[UIColor colorWithRed:0.388 green:0.478 blue:0.568 alpha:0.5] forState:UIControlStateHighlighted];
  [self.button setTitle:@"Show a Fun Fact" forState:UIControlStateNormal];
  [self.button setBackgroundColor:[UIColor whiteColor]];
  [self setBackgroundColour];
  [self.button addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:self.button];
}

- (void) pressed:(id)sender {
  [self setBackgroundColour];
  
  self.textLabel.text = [self.factBook randomFact];
  self.firstTime = true;
  [self firstTimeFunc];
}

- (void) firstTimeFunc {
  if (self.firstTime == true) {
    [self.button setTitle:@"Show another Fun Fact" forState:UIControlStateNormal];
  }
}

- (void) setBackgroundColour {
  UIColor *backgroundColour = [self.colourBook randomColor];
  self.view.backgroundColor = backgroundColour;
  [self.button setTitleColor:backgroundColour forState:UIControlStateNormal];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
  UIStatusBarStyle style = UIStatusBarStyleLightContent;
  return style;
}

- (void) didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

@end
