//
//  main.m
//  Thingy
//
//  Created by Charles Benedict on 27/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
