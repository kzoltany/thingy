//
//  Facts.h
//  Thingy
//
//  Created by Charles Benedict on 27/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Facts : NSObject

@property (strong, nonatomic) NSArray *facts;

- (NSString *) randomFact;

@end
