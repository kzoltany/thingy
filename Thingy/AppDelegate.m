//
//  AppDelegate.m
//  Thingy
//
//  Created by Charles Benedict on 27/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  UIScreen *screen = [UIScreen mainScreen];
  CGRect frame = [screen bounds];
  
  self.window = [[UIWindow alloc] initWithFrame:frame];
  self.window.rootViewController = [[MainViewController alloc] init];
  self.window.backgroundColor = [UIColor colorWithRed:0.388 green:0.478 blue:0.568 alpha:1.0];
  [self.window makeKeyAndVisible];
  return YES;
}

@end
