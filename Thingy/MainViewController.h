//
//  ViewController.h
//  Thingy
//
//  Created by Charles Benedict on 27/02/2015.
//  Copyright (c) 2015 teamrune. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Facts;
@class Colour;

@interface MainViewController : UIViewController

@property (nonatomic) UILabel *textLabel;
@property (nonatomic) UIButton *button;
@property (nonatomic) BOOL firstTime;
@property (strong, nonatomic) Facts *factBook;
@property (strong, nonatomic) Colour *colourBook;

@end

